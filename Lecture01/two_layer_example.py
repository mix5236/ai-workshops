
class FeedForwardTwoLayer:
    @staticmethod
    def input_layer(arr, input_weights):
        print("Input: {}".format(arr))
        print("Input weights: {}".format(input_weights))
        
        input_multiplied = []
        
        for weights in input_weights:
            multiplication = multiplyArrays(weights, arr)
            input_multiplied.append(multiplication)
        
        return input_multiplied
    
    @staticmethod
    def hidden_layer(arr):
        print("Hidden input: {}".format(arr))
        activations = []
        
        for inp in arr:
            relu_result = relu(sumOfElements(inp))
            activations.append(relu_result)
        
        return activations
    
    @staticmethod
    def output_layer(arr, output_weights):    
        print("Output: {}".format(arr))
        print("Output weights: {}".format(output_weights))
        
        multiplication = multiplyArrays(output_weights, arr)
        return sumOfElements(multiplication)
    
input_weights = [[-1, 1], [1, -1]]
inp = FeedForwardTwoLayer.input_layer([25, 4], input_weights)
act = FeedForwardTwoLayer.hidden_layer(inp)
output = FeedForwardTwoLayer.output_layer(act, [-1, 1])
print(output)
